#!/usr/bin/env python

import logging
import os
import pika
import time
import sys
import subprocess
import tempfile
import re
from extractor_config import *
import extractors as extractors
import hyperspy

"""

to install pyclowder :

pip install git+https://opensource.ncsa.illinois.edu/stash/scm/cats/pyclowder.git

"""



def main():
    global extractorName, messageType, rabbitmqExchange, rabbitmqURL, logger

    #set logging
    logging.basicConfig(format='%(levelname)-7s : %(name)s -  %(message)s', level=logging.INFO)
    logging.getLogger('pymedici.extractors').setLevel(logging.DEBUG)
    logger = logging.getLogger(extractorName)
    logger.setLevel(logging.DEBUG)

    n_retries = 0
    while(True):
        try:
            #connect to rabbitmq
            extractors.connect_message_bus(extractorName=extractorName,
                                           messageType=messageType,
                                           processFileFunction=process_file,
                                           rabbitmqExchange=rabbitmqExchange,
                                           rabbitmqURL=rabbitmqURL)
            break
        except pika.exceptions.AMQPConnectionError:
            n_retries += 1
            if n_retries == 5:
                break
            print('Cannot connect to RabbitMQ server. Retry in 5s...')
            time.sleep(5)

def getTif(input_file):
    metadata=dict()
    f=hs.load(input_file)
    if ((hasattr(f.original_metadata, 'fei_metadata'))==True):
        metadata['FEI_metadata']=f.original_metadata.fei_metadata.as_dictionary()
    return metadata

# ----------------------------------------------------------------------
# Process the file and upload the results
def process_file(parameters):

    print(parameters['inputfile'], 'file being processed')
    input_file=parameters['inputfile']
    if (input_file.endswith(".tif" || ".tiff")):
        try:
            metadata_dictionary = getTif(input_file)
        except:
        logger.error("could not retrieve tif metadata " + input_file)
        
    #get metadata as a python dictionary and then upload with this method
    extractors.upload_file_metadata(metadata_dictionary, parameters)

    #create a tempfile of type png, for the image preview
    (fd, tn_file) = tempfile.mkstemp(suffix=".png")
    try:
        #Saves preview to temp file
        f = hs.load(input_file)
        f.savefig(tn_file, overwrite=True)
    except:
        logger.error("could not make preview " + input_file)
    try:
        #Upload Preview & Thumbnail Images
        preview_id = extractors.upload_preview(tn_file, parameters, None)
    except:
        logger.error("could not upload preview " + input_file)
    try:
        upload_preview_title(preview_id, 'preview', parameters)
    except:
        logger.error("could not upload titles " + input_file)
    try:
        extractors.upload_thumbnail(tn_file, parameters)
    except:
        logger.error("could not upload thumbnail " + input_file)
    try:
        os.remove(tn_file)
    except:
        logger.error("Could not remove temporary file")

if __name__ == "__main__":
    main()
