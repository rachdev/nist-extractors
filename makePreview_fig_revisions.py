#New MAKEPREVIEW
def makePreview(input_file, target):
    ftype = str(input_file)
    if ftype.endswith(".dm3", -4):
        f = hs.load(input_file)
        if f.data.ndim < 2:
                img = hs.plot.plot_images(f)
                fig = img.get_figure()
                fig.savefig(target, overwrite=True)
            # else try:
            #     print("signal is 1D")
            #     y_data = f.data
            #     signal_name = f.original_metadata.ImageList.TagGroup0.Name
            #     x_step = float(f.original_metadata.ImageList.TagGroup0.ImageData.Calibrations.Dimension.TagGroup0.Scale)
            #     x_origin = float(
            #         f.original_metadata.ImageList.TagGroup0.ImageData.Calibrations.Dimension.TagGroup0.Origin)
            #     x_units = f.original_metadata.ImageList.TagGroup0.ImageData.Calibrations.Dimension.TagGroup0.Units
            #     x_data = np.arange(x_origin, (len(y_data) * x_step), x_step)
            #     plt.plot(x_data, y_data, 'bo')
            #     plt.grid(True)
            #     plt.title("%s Signal" % signal_name)
            #     plt.xlabel("x axis (%s)" % x_units)
            #     plt.ylabel("Intensity")
            #     plt.savefig(target)
        elif f.data.ndim > 2:
            if (f.data.shape[0] == f.data.shape[1]):
                spec_img=hs.plot.plot_images(f)
                spec_img_fig = spec_img.get_figure()
                spec_img_fig.savefig(target)
            elif (f.data.shape[0] != f.data.shape[1]):
                spec_plot_img=hs.plot.plot_spectra(f, style='heatmap')
                spec_plot_fig=spec_plot_img.get_figure()
                spec_plot_fig.savefig(target, overwrite=True)
            else:
                f.save(target, overwrite=True)

    if ftype.endswith(".ser", -4):
        f = hs.load(input_file)
        if (f.data.shape[0] != f.data.shape[1]):
                img = hs.plot.plot_spectra(f, style='heatmap')
                fig = img.get_figure()
                fig.savefig(target, overwrite=True)
        else:
            f.save(target, overwrite=True)