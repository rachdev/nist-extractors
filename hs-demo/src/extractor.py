import sys
import extractor_config
import extractor_helper
import extractors
import json
import logging
import os
import tempfile
import requests

def main():
    sys.path.insert(0, '../..')
    sys.path.insert(0, '../../..')
    global extractorName, messageType, rabbitmqExchange, rabbitmqURL, registrationEndpoints,logger

    receiver = 'hs.extractor'
    extractorName = extractor_config.extractorName
    messageType = extractor_config.messageType
    rabbitmqExchange = extractor_config.rabbitmqExchange
    rabbitmqURL = extractor_config.rabbitmqURL
    playserverKey = extractor_config.playserverKey


    logging.basicConfig(format='%(levelname)-7s : %(name)s -  %(message)s', level=logging.WARN)
    logging.getLogger('pyclowder.extractors').setLevel(logging.INFO)

    logger = logging.getLogger(receiver)
    logger.setLevel(logging.DEBUG)

    extractors.connect_message_bus(extractorName=extractorName, messageType=messageType, processFileFunction=processFile,
        checkMessageFunction=check_message, rabbitmqExchange=rabbitmqExchange, rabbitmqURL=rabbitmqURL)

def check_message(parameters):
    return True

def processFile(parameters):
    input_file = parameters['inputfile']

    metadata_dictionary = extractor_helper.getMetadata(input_file)
    #get metadata as a python dictionary and then upload with this method
    extractors.upload_file_metadata(metadata_dictionary, parameters)

    #create a tempfile of type png, for the image preview
    (fd, tn_file) = tempfile.mkstemp(suffix=".png")
    try:
        #Saves preview to temp file
        extractor_helper.makePreview(input_file, tn_file)
    except:
        logger.error("could not make preview " + input_file)
    try:
        #Upload Preview & Thumbnail Images
        preview_id = extractors.upload_preview(tn_file, parameters, None)
    except:
        logger.error("could not upload preview " + input_file)
    try:
        upload_preview_title(preview_id, 'preview', parameters)
    except:
        logger.error("could not upload titles " + input_file)
    try:
        extractors.upload_thumbnail(tn_file, parameters)
    except:
        logger.error("could not upload thumbnail " + input_file)
    try:
        os.remove(tn_file)
    except:
        logger.error("Could not remove temporary file")

#this method adds a title to the preview. it was not included with pyclowder
def upload_preview_title(previewid, title_name,parameters):
    key = parameters['secretKey']
    title = dict()
    title['title'] = title_name
    host = parameters['host']
    if (not host.endswith("/")):
        host = host + "/"
    url = host + 'api/previews/'+previewid+'/title?key='+key

    headers = {'Content-Type': 'application/json'}

    if url:
        r = requests.post(url, headers=headers, data=json.dumps(title), verify=False);
