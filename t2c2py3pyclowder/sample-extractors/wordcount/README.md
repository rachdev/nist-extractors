# To build

`docker build -t clowder_wordcount .`

This will build an image called clowder_wordcount

# To run

This assumes you have the clowder stack running using docker-compose. See the docker-compose.yml file in the clowder project.

`docker run -t -i --name clowder_wordcount_1 --rm --link clowder_rabbitmq_1:rabbitmq clowder_wordcount`

This will run the image just created.
--rm will remove the container after finished
--name will name the container clowder_wordcount_1
--link will tell the container the link to rabbitmq so it can add itself to the queue.
-t will make it so the tty is enabled
-i will make it so the shell is interactive

# To add to systemd

The clowder-wordcount.service file is an example that will add the wordcount extractor to systemd allowing you to start it automatically. All you need to do is copy clowder-wordcount.service to /etc/systemd/system and run, edit it to set the parameters for rabbitmq and run the following commands:

```
systemctl enable clowder-wordcount.service
systemctl start clowder-wordcount.service
```

To see the log you can use:

```
journalctl -f -u debod-image.service
```
