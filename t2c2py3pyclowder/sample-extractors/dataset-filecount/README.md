# To build

`docker build -t clowder-mostrecentfile .`

This will build an image called clowder-mostrecentfile

# To run

This assumes you have the clowder stack running using docker-compose. See the docker-compose.yml file in the clowder project.

`docker run -t -i --name clowder-mostrecentfile --rm --link clowder_rabbitmq_1:rabbitmq clowder-mostrecentfile`

This will run the image just created.
--rm will remove the container after finished
--name will name the container clowder_filecount_1
--link will tell the container the link to rabbitmq so it can add itself to the queue.
-t will make it so the tty is enabled
-i will make it so the shell is interactive

# To add to systemd

The clowder-mostrecentfile.service file is an example that will add the clowder-mostrecentfile extractor to systemd allowing you to start it automatically. All you need to do is copy clowder-wordcount.service to /etc/systemd/system and run, edit it to set the parameters for rabbitmq and run the following commands:

```
systemctl enable clowder-mostrecentfile.service
systemctl start clowder-mostrecentfile.service
```

To see the log you can use:

```
journalctl -f -u debod-image.service
```
