import requests
import logging
import time
import json
import tempfile
import os
import pickle

_sslVerify = False


# ----------------------------------------------------------------------
# Prepare file for processing in an HPC environment
def prepare_hpc_processing(extractorName, messageType, processFileFunction, picklefile, checkMessageFunction=None,
                           sslVerify=None):

    global _extractorName, _messageType, _checkMessageFunction, _processFileFunction
    global _sslVerify, _logger
    fileid = 0
    inputfile = None
    logfile = None
    pfile = None

    # Set the global variables
    _extractorName = extractorName  # the name of the extractor
    _messageType = messageType  # the type of messages the extractor will work on
    _checkMessageFunction = checkMessageFunction  # this function decides whether or not to proceed to process the file
    _processFileFunction = processFileFunction  # the function which should be used to process the input file

    _sslVerify = sslVerify

    # Start the logging system if not started before
    logging.basicConfig()
    _logger = logging.getLogger(__name__)

    try:

        pfile = open(picklefile)
        parameters = pickle.load(pfile)

        fileid = parameters['fileid']
        inputfile = parameters['inputfile']
        logfile = parameters['logfile']

        # Tell everybody we are starting to process the file
        status_update(fileid=fileid, status="Started processing file", logfile=logfile)

        # Checks whether to process the file in this message or not
        if _checkMessageFunction is None or _checkMessageFunction(parameters):
            if _processFileFunction:
                _processFileFunction(parameters)

        status_update(fileid=fileid, status="Done", logfile=logfile)

    except:
        _logger.exception("[%s] error processing", fileid)
        status_update(fileid=fileid, status="Error processing", logfile=logfile)
        status_update(fileid=fileid, status="Done", logfile=logfile)

    finally:

        if inputfile is not None:
            try:
                os.remove(inputfile)
            except OSError:
                pass
            except UnboundLocalError:
                pass

            try:
                pfile.close()
            except IOError:
                pass


# ------------------
# Store updates about status of processing file
def status_update(status, fileid, logfile):
    """Store notification on log file with update"""

    global _extractorName, _logger
    log = None

    _logger.debug("[%s] : %s", fileid, status)

    statusreport = dict()
    statusreport['file_id'] = fileid
    statusreport['extractor_id'] = _extractorName
    statusreport['status'] = status
    statusreport['start'] = time.strftime('%Y-%m-%dT%H:%M:%S')

    if logfile is not None and os.path.isfile(logfile) is True:
        try:
            log = open(logfile, 'a')
            log.write(json.dumps(statusreport) + '\n')
            log.close()
        except IOError:
            _logger.exception("Error: unable to write extractor status to log file")
            raise
        except:
            _logger.exception("Error: unable to write extractor status to log file")
            raise
        finally:
            log.close()
            pass
            

#
# def status_update(status, parameters):
#   """Send notification on message bus with update"""

# _status_update(status=status, fileid=parameters['fileid'], channel=parameters['channel'],
#               header=parameters['header'])


# ----------------------------------------------------------------------
# Upload file metadata to Clowder
def _upload_file_metadata(fileid, metadata, host, key, logfile):
    """Upload file metadata to Clowder"""

    global _sslVerify

    status_update(fileid=fileid, status="Uploading file metadata.", logfile=logfile)

    headers = {'Content-Type': 'application/json'}

    if not host.endswith("/"):
        host += "/"

    url = host + 'api/files/' + fileid + '/metadata?key=' + key
    
    r = requests.post(url, headers=headers, data=json.dumps(metadata), verify=_sslVerify)
    r.raise_for_status()


def upload_file_metadata(mdata, parameters):
    """Upload file metadata to Clowder"""

    return _upload_file_metadata(fileid=parameters['fileid'], metadata=mdata, host=parameters['host'],
                                 key=parameters['secretKey'], logfile=parameters['logfile'])


# ----------------------------------------------------------------------
# Upload file metadata to Clowder - JSON-LD version
def _upload_file_metadata_jsonld(fileid, metadata, host, key, logfile):
    """Upload file metadata to Clowder"""

    global _sslVerify

    status_update(fileid=fileid, status="Uploading file metadata.", logfile=logfile)

    headers = {'Content-Type': 'application/json'}

    if not host.endswith("/"):
        host += "/"

    url = host + 'api/files/' + fileid + '/metadata.jsonld?key=' + key
    r = requests.post(url, headers=headers, data=json.dumps(metadata), verify=_sslVerify)
    r.raise_for_status()


def upload_file_metadata_jsonld(mdata, parameters):
    """Upload file metadata to Clowder"""

    return _upload_file_metadata_jsonld(fileid=parameters['fileid'], metadata=mdata, host=parameters['host'],
                                        key=parameters['secretKey'], logfile=parameters['logfile'])


# ----------------------------------------------------------------------
# Upload file tags to Clowder
def _upload_file_tags(fileid, tags, host, key, logfile):
    """Upload file tags to Clowder"""

    global _sslVerify

    status_update(fileid=fileid, status="Uploading file tags.", logfile=logfile)

    headers = {'Content-Type': 'application/json'}

    if not host.endswith("/"):
        host += "/"

    url = host + 'api/files/' + fileid + '/tags?key=' + key

    r = requests.post(url, headers=headers, data=json.dumps(tags), verify=_sslVerify)
    r.raise_for_status()


def upload_file_tags(tags, parameters):
    """Upload file tags to Clowder"""

    return _upload_file_tags(tags=tags, host=parameters['host'], key=parameters['secretKey'],
                             fileid=parameters['fileid'], logfile=parameters['logfile'])


# ----------------------------------------------------------------------
# Upload section tags to Clowder
def _upload_section_tags(sectionid, tags, host, key, logfile):
    """Upload section tags to Clowder"""

    global _sslVerify

    headers = {'Content-Type': 'application/json'}

    if not host.endswith("/"):
        host += "/"

    url = host + 'api/sections/' + sectionid + '/tags?key=' + key

    r = requests.post(url, headers=headers, data=json.dumps(tags), verify=_sslVerify)
    r.raise_for_status()


def upload_section_tags(sectionid, tags, parameters):
    """Upload section tags to Clowder"""

    return _upload_section_tags(sectionid=sectionid, tags=tags, host=parameters['host'], key=parameters['secretKey'],
                                logfile=parameters['logfile'])


# ----------------------------------------------------------------------
# Upload a preview to Clowder
def upload_preview(previewfile, parameters, previewdata=None):
    """Upload preview to Clowder
       previewfile: the file containing the preview
       parameters: parameters given by pyClowder
       previewdata: any metadata to be associated with preview,
                    this can contain a section_id to indicate the
                    section this preview should be associated with.
    """
    global _sslVerify, _logger

    key = parameters['secretKey']
    host = parameters['host']

    if not host.endswith("/"):
        host += "/"
    url = host + 'api/previews?key=' + key
    
    # upload preview
    with open(previewfile, 'rb') as f:
        files = {"File": f}
        r = requests.post(url, files=files, verify=_sslVerify)
        r.raise_for_status()

    previewid = r.json()['id']
    _logger.debug("preview id = [%s]", previewid)

    # associate uploaded preview with original file/collection
    headers = {'Content-Type': 'application/json'}
    url = None
    if previewdata and previewdata['section_id']:
        url = None
    elif parameters['fileid']:
        url = host + 'api/files/' + parameters['fileid'] + '/previews/' + previewid + '?key=' + key
    elif parameters['collectionid']:
        url = host + 'api/collections/' + parameters['collectionid'] + '/previews/' + previewid + '?key=' + key
    if url:
        r = requests.post(url, headers=headers, data=json.dumps({}), verify=_sslVerify)
        r.raise_for_status()

    # associate metadata with preview
    if previewdata is not None:
        url = host + 'api/previews/' + previewid + '/metadata?key=' + key
        r = requests.post(url, headers=headers, data=json.dumps(previewdata), verify=_sslVerify)
        r.raise_for_status()

    return previewid


# ----------------------------------------------------------------------
# Upload a thumbnail to Clowder
def upload_thumbnail(thumbnail, parameters):
    """Upload thumbnail to Clowder, currently only files are supported.
       thumbnail: the file containing the thumbnail
       parameters: parameters given by pyClowder
    """
    global _sslVerify, _logger

    key = parameters['secretKey']
    host = parameters['host']

    if not host.endswith("/"):
        host += "/"

    url = host + 'api/fileThumbnail?key=' + key
    
    # upload preview
    with open(thumbnail, 'rb') as f:
        files = {"File": f}
        r = requests.post(url, files=files, verify=_sslVerify)
        r.raise_for_status()
    thumbnailid = r.json()['id']
    _logger.debug("preview id = [%s]", thumbnailid)

    # associate uploaded preview with original file/dataset
    headers = {'Content-Type': 'application/json'}
    url = None
    if parameters['fileid']:
        url = host + 'api/files/' + parameters['fileid'] + '/thumbnails/' + thumbnailid + '?key=' + key
    if url:
        r = requests.post(url, headers=headers, data=json.dumps({}), verify=_sslVerify)
        r.raise_for_status()
    else:
        raise

    return thumbnailid


# ----------------------------------------------------------------------
# Upload a section to Clowder
def _upload_section(sectiondata, host, key, logfile):
    """Upload file preview to Clowder"""

    global _sslVerify, _logger

    # status_update(fileid=fileid, status="Uploading section.")
                
    if not host.endswith("/"):
        host += "/"
                
    url = host + 'api/sections?key=' + key
    
    headers = {'Content-Type': 'application/json'}
   
    r = requests.post(url, headers=headers, data=json.dumps(sectiondata), verify=_sslVerify)
    r.raise_for_status()
    
    sectionid = r.json()['id']

    _logger.debug("section id = [%s]", sectionid)

    return sectionid


def upload_section(sectiondata, parameters):
    """Upload file preview to Clowder"""

    return _upload_section(sectiondata=sectiondata, host=parameters['host'], key=parameters['secretKey'],
                           logfile=parameters['logfile'])


# ----------------------------------------------------------------------
# Upload a file to dataset in Clowder
def _upload_file_to_dataset(filepath, datasetid, host, key, logfile):
    """Upload a file to a dataset in Clowder"""

    global _sslVerify, _logger

    if not host.endswith("/"):
        host += "/"

    url = host + 'api/uploadToDataset/' + datasetid + '?key=' + key

    r = requests.post(url, files={"File": open(filepath, 'rb')}, verify=_sslVerify)
    r.raise_for_status()

    uploadedfileid = r.json()['id']
    _logger.debug("uploaded file id = [%s]", uploadedfileid)

    return uploadedfileid


def upload_file_to_dataset(filepath, parameters):
    """Upload file to Clowder dataset"""

    return _upload_file_to_dataset(filepath=filepath, datasetid=parameters['datasetId'], host=parameters['host'],
                                   key=parameters['secretKey'], logfile=parameters['logfile'])


# ----------------------------------------------------------------------
# Download file from Clowder
def download_file(host, key, fileid, intermediatefileid, ext, logfile):
    """Download file to be processed from Clowder"""

    global _sslVerify

    status_update(fileid=fileid, status="Downloading file.", logfile=logfile)

    # fetch data
    if not host.endswith("/"):
        host += "/"

    url = host + 'api/files/' + intermediatefileid + '?key=' + key
    r = requests.get(url, stream=True, verify=_sslVerify)
    r.raise_for_status()

    (fd, inputfile) = tempfile.mkstemp(suffix=ext)
    with os.fdopen(fd, "w") as f:
        for chunk in r.iter_content(chunk_size=10*1024):
            f.write(chunk)
    return inputfile


# ----------------------------------------------------------------------
# Get Clowder file URL
def get_file_url(fileid, parameters):
    host = parameters['host']
    if not host.endswith("/"):
        host += "/"
    return host + "files/" + fileid
