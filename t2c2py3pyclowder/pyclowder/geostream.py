import json
import logging
import requests

# cached information
_sensors = None
_streams = None

# ----------------------------------------------------------------------
# find the sensor in Clowder
# ----------------------------------------------------------------------
def get_sensor(clowder_url, secret_key, sensor_type,
               sensor_name=None, sensor_popup=None):
  global _sensors

  log = logging.getLogger(__name__)

  # get all sensors
  if _sensors is None:
    r = requests.get('%sapi/geostreams/sensors?key=%s' %
                     (clowder_url, secret_key))
    if r.status_code != 200:
      log.error('Could not get list of sensors : [%d] - %s)' %
                (r.status_code, r.text))
      return None
    result = r.json()
    if isinstance(result, list):
      _sensors = result
    else:
      _sensors = []

  # find the right sensor
  for s in _sensors:
    if s['properties']['type']['id'] == sensor_type:
      if sensor_name is not None and s['name'] == sensor_name:
        return s
      if (sensor_popup is not None and
          s['properties']['popupContent'].find(sensor_popup) != -1):
        return s

  # sensor not found
  log.error('Could not find sensor')
  return None

# ----------------------------------------------------------------------
# find/create the stream in Clowder
# ----------------------------------------------------------------------
def get_stream(clowder_url, secret_key, sensor_id, stream_name,
               geometry=None, properties={}):
  """Finds or creats a stream in Clowder.

  If the stream with the given name does not exist yet, it will
  be created. In both cases the stream is returned.

  Args:
      clowder_url: url of clowder, should end with a /.
      secret_key: key to access clowder.
      sensor_id: the sensor for the stream
      stream_name: name of stream to find/create.
      geometry: geolocation of the stream.
      properties: properties to be associated with the stream.

  Returns:
      The stream found/created or None if it could not be created.

  Raises:
      HttpException: An error occured accessing clowder.
  """
  global _streams

  log = logging.getLogger(__name__)

  # get all streams
  if _streams is None:
    # check to see if stream exists
    r = requests.get('%sapi/geostreams/streams?key=%s' %
                     (clowder_url, secret_key))
    if (r.status_code != 200):
      log.error('Could not get list of streams : [%d] - %s)' %
                (r.status_code, r.text))
      return None
    result = r.json()
    if isinstance(result, list):
      _streams = result
    else:
      _streams = []

  # find the stream 
  for c in _streams:
    if c['name'] == stream_name and c['sensor_id'] == str(sensor_id):
      return c

  # create stream if need be
  if geometry == None or len(properties) == 0:
    return None

  # create the stream
  body = {'name': stream_name,
          'type': 'Feature',
          'geometry': {'coordinates': geometry},
          'properties': properties,
          'sensor_id': str(sensor_id)}
  headers = {'Content-type': 'application/json'}
  r = requests.post('%sapi/geostreams/streams?key=%s' %
                    (clowder_url, secret_key),
                    data = json.dumps(body), headers=headers)
  if r.status_code != 200:
    log.error('Problem creating stream : [%d] - %s)' %
              (r.status_code, r.text))
    return None
  stream_id = r.json()['id']
  log.info('Could not find stream "%s" created it with id %s.' % 
           (stream_name, stream_id))

  # fetch stream and return
  r = requests.get('%sapi/geostreams/streams/%s?key=%s' %
                   (clowder_url, stream_id, secret_key))
  if (r.status_code != 200):
    log.error('Could not get the stream : [%d] - %s)' %
              (r.status_code, r.text))
    return None
  stream = r.json()
  _streams.append(stream)
  return stream

# ----------------------------------------------------------------------
# update the sensors
# ----------------------------------------------------------------------
def update_sensors(clowder_url, secret_key, sensor_id=None):  
  log = logging.getLogger(__name__)

  if sensor_id is None:
    # update all sensors
    r = requests.get('%sapi/geostreams/sensors/update?key=%s' %
                     (clowder_url, secret_key))
    if r.status_code != 200:
      log.error('Problem updating all sensors  : [%d] - %s)' %
                 (r.status_code, r.text))
    else:
      log.info('Updated all sensors.')
  else:
    # update specific sensors
    r = requests.get('%sapi/geostreams/sensors/%d/update?key=%s' %
                     (clowder_url, sensor_id, secret_key))
    if r.status_code != 200:
      log.error('Problem updating sensors %d : [%d] - %s)' %
                 (sensor_id, r.status_code, r.text))
    else:
      log.info('Updated all sensors.')

# ----------------------------------------------------------------------
# upload file to Clowder, this is needed to create dataset
# ----------------------------------------------------------------------
def add_datapoint(clowder_url, secret_key, sensor, stream_id,
                  start_date, end_date=None, properties={}):
  """Finds or creats a stream in Clowder.

  If the collectin with the given name does not exist yet, it will
  be craeted. In both cases the id of the collection is returned.

  Args:
      clowder_url: url of clowder, should end with a /.
      secret_key: key to access clowder.
      sensor: the sensor for the stream
      stream: name of stream to find/create.
      properties: properties to be associated with the stream.

  Returns:
      The id of stream found/created or None if it could not be
      created.

  Raises:
      HttpException: An error occured accessing clowder.
  """
  log = logging.getLogger(__name__)

  if end_date is None:
    end_date = start_date
  body = {'start_time': start_date,
          'end_time': end_date,
          'type': 'Feature',
          'geometry': {'coordinates': sensor['geometry']['coordinates']},
          'properties': properties,
          'stream_id': str(stream_id)}
  headers = {'Content-type': 'application/json'}
  r = requests.post('%sapi/geostreams/datapoints?key=%s' % 
                    (clowder_url, secret_key),
                    data=json.dumps(body), headers=headers)
  if r.status_code != 200:
    log.error('Problem adding data to stream : [%d] - %s)' %
              (r.status_code, r.text))
    return None
  if r.text != '"success"':
    log.error('Problem adding data to stream : %s' % r.text)
    return None
  return 1

# ----------------------------------------------------------------------
