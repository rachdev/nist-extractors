# name to show in rabbitmq queue list
extractorName="new.basic.extractorPython3"

# URL to be used for connecting to rabbitmq
#rabbitmqURL = "amqp://guest:guest@localhost/%2f"
rabbitmqURL="amqp://guest:guest@localhost:5672/%2f"
#rabbitmqURL="amqp://guest:guest@192.168.3.153:5672/%2f"

# name of rabbitmq exchange
rabbitmqExchange="clowder"
playserverKey='phuong-test'

# type of files to process
messageType='*.file.image.#'

# trust certificates, set this to false for self signed certificates
sslVerify=False

# image generating binary, or None if none is to be generated
imageBinary = "/usr/local/bin/convert"

# image preview type
imageType = "png"

# image thumbnail command line
imageThumbnail = "@BINARY@ @INPUT@ -resize 225^ @OUTPUT@"

# image preview command line
imagePreview = "@BINARY@ @INPUT@ -resize 800x600 @OUTPUT@"

# type specific preview, or None if none is to be generated
previewBinary = None

# type preview type
previewType = None

# type preview command line
previewCommand = None